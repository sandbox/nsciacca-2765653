<?php
/**
 * @file
 * Metatag integration for the metatag_jsonld module.
 */

/**
 * Implements hook_metatag_info().
 */
function metatag_jsonld_metatag_info() {
  $info['groups']['metatag-jsonld'] = array(
    'label' => t('JSON-LD'),
    'description' => t('JSON-LD is a lightweight Linked Data format.'),
    'form' => array(
      '#weight' => 81,
    ),
  );

  // JSON-LD meta tags stack after the Twitter Cards tags.
  $weight = 80;

  // Additional tags.
  $defaults = array(
    'class' => 'DrupalTextMetaTag',
    'group' => 'json-ld',
    'element' => array(
      '#theme' => 'metatag_jsonld',
    ),
  );

  $info['tags']['jsonld'] = array(
    'label' => t('JSON-LD'),
    'description' => t('Enter the JSON-LD text with token support.'),
    'weight' => ++$weight,
    'form' => array(
      '#type' => 'textarea',
      '#rows' => 4,
      '#wysiwyg' => FALSE,
      '#maxlength' => NULL,
    ),
  ) + $defaults;


  return $info;
}
